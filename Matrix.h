#ifndef MATRIX_H
#define MATRIX_H

class Matrix{
    private:
        //Tamaño de la matriz n*n
        int n;
    public:
        //Se instancian las funciones
        Matrix();
        Matrix(int n);
        int** agregar_datos(int**matriz);
        void mostrar_matrix(int** matriz);
        void inicializar_matriz_enteros(int **matriz);
        void inicializar_vector_caracter(string *vector);
        int** crear_matrix();
};
#endif