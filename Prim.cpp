#include <cstring>
#include <iostream>
#include <fstream>
using namespace std;
#include "Matrix.h"

#define INF 9999999
/* #define V 6 */

/*
9-75-0-0-95-19-42-51-66-31 Esta sucesión de números genera el grafo de la siguiente matriz
int G[V][V] = {
    {0, 9, 75, 0, 0},
    {9, 0, 95, 19, 42},
    {75, 95, 0, 51, 66},
    {0, 19, 51, 0, 31},
    {0, 42, 66, 31, 0}};
*/

/*
1-3-0-0-3-6-0-4-2-5 Esta sucesión de números genera el grafo de la siguiente matriz
int G[V][V] = {
    {0, 1, 3, 0, 0},
    {1, 0, 3, 6, 0},
    {3, 3, 0, 4, 2},
    {0, 6, 4, 0, 5},
    {0, 0, 2, 5, 0}};
*/

/* 
6-1-5-0-0-5-0-3-0-5-6-4-0-2-6 Esta sucesión de números genera el grafo de la siguiente matriz
int G[V][V] = {
    {0, 6, 1, 5, 0, 0},
    {6, 0, 5, 0, 3, 0},
    {1, 5, 0, 5, 6, 4},
    {5, 0, 5, 0, 0, 2},
    {0, 3, 6, 0, 0, 6},
    {0, 0, 4, 2, 6, 0}}; */

//Función que imprime el grafo con los datos obtenidos del algoritmo de Prim
void grafo_prim(int *inicio, int *fin, int *pesos, char *vector, int n){
    FILE *fp;
    fp = fopen("prim.txt", "w");
    fprintf(fp, "%s\n", "graph G {");
    fprintf(fp, "%s\n", "graph [rankdir=LR]");
    fprintf(fp, "%s\n", "node [style=filled fillcolor=cyan];");
  
    for (int i=0; i<(n-1); i++) {
        fprintf(fp, "%c%s%c [label=%d];\n", vector[inicio[i]],"--", vector[fin[i]], pesos[i]);
    }
    fprintf(fp, "%s\n", "}");
    fclose(fp);

    //Teniendo el archivo txt creado, se crea y se abre el grafo
    system("dot -Tpng -oprim.png prim.txt");
    system("eog prim.png &");
}

//Función que imprime el grafo de la matriz original
void imprimir_grafo(int **matriz, char *vector, int n){
    FILE *fp;
    
    //Se forma un archivo txt con la información necesaria de la matriz para luego crear el grafo
    fp = fopen("grafo.txt", "w");
    fprintf(fp, "%s\n", "graph G {");
    fprintf(fp, "%s\n", "graph [rankdir=LR]");
    fprintf(fp, "%s\n", "node [style=filled fillcolor=cyan];");
  
    for (int i=0; i<n; i++) {
        for (int j=0; j<n; j++) {
            if (i < j) {
                if (matriz[i][j] > 0) {
                fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"--", vector[j], matriz[i][j]);
                }
            }
        }
    }
    fprintf(fp, "%s\n", "}");
    fclose(fp);
    //Teniendo el archivo txt creado, se crea y se abre el grafo
    system("dot -Tpng -ografo.png grafo.txt");
    system("eog grafo.png &");
}

//Función principal
int main(int argc, char **argv){
    system("clear");
    //Convirte el parametro pasado por el usuario a un entero
    int V = atoi(argv[1]);
    //Si el tamaño es 2 o menor, termina el programa
    if (V < 3){
        cout << "El tamaño debe ser mayor a 2" << endl;
        exit(-1);
    }
    //Creación de la matriz y arreglos necesarios
    Matrix matrix = Matrix(V);
    int** G = matrix.crear_matrix();
    matrix.agregar_datos(G);
    int no_edge;
    int *v_inicial;
    int *v_final;
    int *pesos;
    v_inicial = new int[V];
    v_final = new int[V];
    pesos = new int[V];

    //Creación de los nombres de los vértices (partiendo desde "a")
    char names[V];
    int inicio = 97;  
    for (int i=0; i<V; i++) {
        names[i] = inicio+i;
    }

    //Crea un arreglo para marcar el vértice seleccionado
    int selected[V];
    memset(selected, false, sizeof(selected));

    //Número del vértice, se inicia en 0 (siempre será menor a V-1)
    no_edge = 0;
    selected[0] = true;

    int x; //Posición de la fila
    int y; //Posición de la columna
    cout << "---------------- PRIM ----------------" << endl;
    cout << "Vertices  :  Peso  :  L{}" << endl;
    //Se implementa el algoritmo de Prim
    while (no_edge < V - 1) {

        int min = INF;
        x = 0;
        y = 0;

        for (int i = 0; i < V; i++) {
            if (selected[i]) {
                for (int j = 0; j < V; j++) {
                    if (!selected[j] && G[i][j]) { 
                        if (min > G[i][j]) {
                            min = G[i][j];
                            x = i;
                            y = j;
                        }
                    }
                }
            }
        }
        if(x != y){
            //Se guarda el peso, la posición X como el vector inicial, y la posición como el vector final
            pesos[no_edge] = G[x][y];
            v_inicial[no_edge] = x;
            v_final[no_edge] = y;
        }
        //Se imprimen los datos obtenidos por la terminal
        cout << names[x] << " - " << names[y] << "     :  " << G[x][y] << "     : ";
        selected[y] = true;
        no_edge++;
        for(int i = 0; i < no_edge; i++){
        cout << " (" << names[v_inicial[i]] << ", " << names[v_final[i]] << ")";
        }
        cout << endl;
    }
    //Se muestran los grafos de la matriz original y de los datos obtenidos por el algoritmo de Prim
    imprimir_grafo(G, names, V);
    grafo_prim(v_inicial, v_final, pesos, names, V);

    return 0;
}
