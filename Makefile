prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Prim.cpp Matrix.cpp
OBJ = Prim.o Matrix.o
APP = prim

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

.PHONY: install	
