# Guía 7 - Unidad II

### Descripción
Este algoritmo consiste en la implementación del algoritmo de Prim para encontrar un árbol abarcador de costo mínimo, dada una matriz bidimensional inicial. Los datos de la matriz son ingresados por el usuario manualmente, a excepción de la diagonal de la matriz y todos los números bajo la diagonal, ya que son números redundantes (cuando el usuario llena la parte superior de la matriz, la parte inferior se llena automáticamente de forma simétrica).  

### Ejecución
Luego de descargar los archivos contenidos en este repositorio, el usuario debe escribir vía terminal el comando "make" para compilar los archivos. Esto dejará un ejecutable llamado "prim", el cual recibe un parámetro inicial de tipo entero luego de escribir el nombre del programa (ejemplo: ./prim 4). El comando anterior iniciará el programa creando una matriz bidimensional de tamaño 4x4. El usuario debe ingresar un parametro mayor o igual a 3, de lo contrario, el programa terminará.

### Luego de ejecutar
Si se ha ejecutado el programa correctamente, se le pedirá al usuario las distancias de cada vector respecto al vector inicial. Las diagonales no se le piden al usuario ya que se llenan con 0 de forma predeterminada. Habiendo llenado la matriz con los datos a elección del usuario, el programa comenzará a implementar el algoritmo de Prim, mostrando por terminal los vectores encontrados por el algoritmo, los costos entre cada vector, y la lista L={} que contiene las listas de vectores cuya relación tiene el menor costo en el grafo. Por último, se mostrarán dos imágenes, una con el grafo de la matriz inicial, y otra con el grafo de costo mínimo con los datos obtenidos por el algoritmo de Prim.
Al finalizar el programa, debería quedar por terminal los datos del algoritmo de Prim, y las imágenes de los dos grafos.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
